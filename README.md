# Usersync

A tool to manage Identities and their roles.

With this tool it is possible to manage all your identities (in a company context, the employees) in one single place with all
the attributes they need (for example an employee-number).

In your company you will have many authentication systems which needs a username, a password and a groupmapping to 
authenticate users. Normaly this is done with the help of an LDAP or an Active Directory system. If your company 
grows larger you will need also some more authentication systems and you have the need to syncronize the users from
one single point to all these systems.

This is where usersync join the party. With usersync you can manage all your identities at one single point and deploy
the needed identities to your authentication systems.

Some people might call it an IDM.

While usersync is not a full featured IDM (yet) you can already use it to syncronize the users from it to different systems
like Active Directory, LDAP, FreeIPA, Google, ...

## Technical Things

In your company you will have an ORG-chart somewhere and normaly you use this org-chart also in your access management.
Imagine that org-chart:

```
company
  +--+ Org1
  |  +--+ Org1Sub1
  |  |  +--+ User1
  |  |  |  + User2
  |  +--+ User3
  +--+ Org2
     +--+ User4
```

Now, you have (for example) a sharepoint system where you want to give all the people belonging to *Org1* access to some pages.
Normaly you need to add all the Sub-Orgs to the permission-table inside sharepoint. 

... bla bla bla ...

### Database design

An ORG-Chart in your company normaly displays the permissions people have in a very good way. But the problem is, most 
systems like Windows, Linux, CMS, $Software doesn't know anything from nested permissions.

Usersync will solve this problem for you. It will add all the people in SubOrgs to the Orgs above and vice-versa.

This org-chart will result in the following ldap groups.
```
company
  +--+ Org1
  |  +--+ Org1Sub1
  |  |  +--+ User1
  |  |  |  + User2
  |  +--+ Org1Sub2
  |  |  |  + User5
  |  |  |  + User6
  |  +--+ User3
  +--+ Org2
     +--+ User4
```

```
* cn=company
* cn=Org1
* cn=Org1Sub1
* cn=Org2

* uid=User1
  memberOf=Org1Sub1
  memberOf=Org1
  memberOf=company
* uid=User2
  memberOf=Org1Sub1
  memberOf=Org1
  memberOf=company
* uid=User3
  memberOf=Org1Sub1
  memberOf=Org1Sub2
  memberOf=Org1
  memberOf=company
* uid=User4
  memberOf=Org2
  memberOf=company
* uid=User5
  memberOf=Org1Sub2
  memberOf=Org1
  memberOf=company
* uid=User6
  memberOf=Org1Sub2
  memberOf=Org1
  memberOf=company
```

Here you will notice, that *User3* is also part of its subgroups (or orgs) because, a person (like a teamlead) should also have
access to the pages of his sub-orgs. 

So in the database everything will be multidimensional. 

We will use PostgreSQL as storage backend. And the database will use nestedsets to store the hierarchy.


#### Objects

Every item stored in the database is an *Obeject*. An objects has the attribute *type* which can be used to filter special 
objects by the syncronization process to your authentication-systems.
An object has also many attributes. These Attributes can be freely managed.

See: *lib/Rex/Usersync/Schema/Result/*

```
 ----------- -------------
| object_id | name        |
 ----------- -------------
|      1    | user        |
 ----------- -------------
|      2    | org         |
 ----------- -------------
|      3    | group       |
 ----------- -------------
|      4    | mailinglist |
 ----------- -------------
```


#### Attributes

Attributes are things for an object that holds a value. For example an employeeNumber, E-Mail, ...

```
 -------------- ------------ ------------------------------ ---------------
| attribute_id | key        |  syntax                      |   encrypted   |
 -------------- ------------ ------------------------------ ---------------
|       1      | employeeNo |  Integer                     |      0        |
 -------------- ------------ ------------------------------ ---------------
|       2      | email      |  EMail                       |      0        |
 -------------- ------------ ------------------------------ ---------------
|       3      | password   | My::Custom::Syntax::Checker  |      1        |
 -------------- ------------ ------------------------------ ---------------
|       4      | name       | String                       |      0        |
 -------------- ------------ ------------------------------ ---------------
```

Here you see the *syntax* column. The content of this column directly matches a Perl class name. So when the validation
process is called, it will get this value out of the table and load the given class. It will call the *validate()* mehtod
of this class. If the validation is good, the *validate()* method must return the value (so it would also be possible 
to do cleanups of the given value). If the validation is wrong, it must throw an exception of type *ValidationFailedException*.

#### ObjectAttributes

This class (table) is responsible to map the *objects* to their *attributes*. 
It just consists of a *object_id* and *attribute_id* field. This is mainly for UIs so that they know which attributes
they need to display on the *add* object page.


#### ObjectConfiguration

This class (table) is responsible to store all the data.

```
 ------------------------- --------------------- ----------- -------------- ---------------------------------
| object_configuration_id | ref_id              | object_id | attribute_id | value                           | 
 ------------------------- --------------------- ----------- -------------- ---------------------------------
| 1                       | abcd-1234-4321-0001 | 1         | 1            | 100                             |
 ------------------------- --------------------- ----------- -------------- ---------------------------------
| 2                       | abcd-1234-4321-0001 | 1         | 2            | user1@company.tld               |
 ------------------------- --------------------- ----------- -------------- ---------------------------------
| 3                       | abcd-1234-4321-0001 | 1         | 3            | CRYPTEDTHINGS                   |
 ------------------------- --------------------- ----------- -------------- ---------------------------------
| 4                       | abcd-1234-4321-0010 | 2         | 4            | company                         |
 ------------------------- --------------------- ----------- -------------- ---------------------------------
| 5                       | abcd-1234-4321-0015 | 2         | 4            | Org1                            |
 ------------------------- --------------------- ----------- -------------- ---------------------------------
| 6                       | abcd-1234-4321-0020 | 2         | 4            | Org2                            |
 ------------------------- --------------------- ----------- -------------- ---------------------------------
| 7                       | abcd-1234-4321-0025 | 2         | 4            | Org1Sub1                        |
 ------------------------- --------------------- ----------- -------------- ---------------------------------
| 8                       | abcd-1234-4321-0026 | 2         | 4            | Org1Sub2                        |
 ------------------------- --------------------- ----------- -------------- ---------------------------------
| 9                       | abcd-1234-4321-0100 | 1         | 1            | 101                             |
 ------------------------- --------------------- ----------- -------------- ---------------------------------
| 10                      | abcd-1234-4321-0100 | 1         | 2            | user2@company.tld               |
 ------------------------- --------------------- ----------- -------------- ---------------------------------
| 11                      | abcd-1234-4321-0100 | 1         | 3            | CRYPTEDTHINGS                   |
 ------------------------- --------------------- ----------- -------------- ---------------------------------
| 12                      | abcd-1234-4321-0110 | 1         | 1            | 102                             |
 ------------------------- --------------------- ----------- -------------- ---------------------------------
| 13                      | abcd-1234-4321-0110 | 1         | 2            | user3@company.tld               |
 ------------------------- --------------------- ----------- -------------- ---------------------------------
| 14                      | abcd-1234-4321-0110 | 1         | 3            | CRYPTEDTHINGS                   |
 ------------------------- --------------------- ----------- -------------- ---------------------------------
| 15                      | abcd-1234-4321-0120 | 1         | 1            | 103                             |
 ------------------------- --------------------- ----------- -------------- ---------------------------------
| 16                      | abcd-1234-4321-0120 | 1         | 2            | user4@company.tld               |
 ------------------------- --------------------- ----------- -------------- ---------------------------------
| 17                      | abcd-1234-4321-0120 | 1         | 3            | CRYPTEDTHINGS                   |
 ------------------------- --------------------- ----------- -------------- ---------------------------------
| 18                      | abcd-1234-4321-0130 | 1         | 1            | 104                             |
 ------------------------- --------------------- ----------- -------------- ---------------------------------
| 19                      | abcd-1234-4321-0130 | 1         | 2            | user5@company.tld               |
 ------------------------- --------------------- ----------- -------------- ---------------------------------
| 20                      | abcd-1234-4321-0130 | 1         | 3            | CRYPTEDTHINGS                   |
 ------------------------- --------------------- ----------- -------------- ---------------------------------
| 21                      | abcd-1234-4321-0140 | 1         | 1            | 105                             |
 ------------------------- --------------------- ----------- -------------- ---------------------------------
| 22                      | abcd-1234-4321-0140 | 1         | 2            | user6@company.tld               |
 ------------------------- --------------------- ----------- -------------- ---------------------------------
| 23                      | abcd-1234-4321-0140 | 1         | 3            | CRYPTEDTHINGS                   |
 ------------------------- --------------------- ----------- -------------- ---------------------------------
```

As you can see, this table holds all the information for one user (internally reffered as *abcd-1234-4321-0001*). 
The employeeNo is *100*  the email is *user1@company.tld* and the password is *CRYPTEDTHINGS*.

#### ObjectTree

Now, after we have all the meta things and the data, we need a table to store the org/permission chart. For this
we're using nested sets with the level extension, so it is pretty easy to get all the children or the ancestors
of an object. 

```
 ---------------- ----- ----- ------- --------- --------------------------
| object_tree_id | lft | rgt | level | tree_id | ref_id                   |  
 ---------------- ----- ----- ------- --------- --------------------------
| 1              | 1   | 21  | 0     | 1       | abcd-1234-4321-0010      |  
 ---------------- ----- ----- ------- --------- --------------------------
| 2              | 2   | 17  | 1     | 1       | abcd-1234-4321-0015      |  
 ---------------- ----- ----- ------- --------- --------------------------
| 3              | 3   | 8   | 2     | 1       | abcd-1234-4321-0025      |  
 ---------------- ----- ----- ------- --------- --------------------------
| 4              | 4   | 5   | 3     | 1       | abcd-1234-4321-0001      |  
 ---------------- ----- ----- ------- --------- --------------------------
| 5              | 6   | 7   | 3     | 1       | abcd-1234-4321-0100      |  
 ---------------- ----- ----- ------- --------- --------------------------
| 6              | 9   | 14  | 2     | 1       | abcd-1234-4321-0026      |  
 ---------------- ----- ----- ------- --------- --------------------------
| 7              | 10  | 11  | 3     | 1       | abcd-1234-4321-0130      |  
 ---------------- ----- ----- ------- --------- --------------------------
| 8              | 12  | 13  | 3     | 1       | abcd-1234-4321-0140      |  
 ---------------- ----- ----- ------- --------- --------------------------
| 9              | 15  | 16  | 2     | 1       | abcd-1234-4321-0110      |
 ---------------- ----- ----- ------- --------- --------------------------
| 10             | 18  | 21  | 2     | 1       | abcd-1234-4321-0020      |  
 ---------------- ----- ----- ------- --------- --------------------------
| 11             | 19  | 20  | 3     | 1       | abcd-1234-4321-0120      |  
 ---------------- ----- ----- ------- --------- --------------------------
```

You can have multiple trees. For example if you need to manage projects in your company with different org-chart, you can
add a special tree for this project.

All trees will be synced and combined.

### Configuration

#### Resources

A resource is a authentication system. A system where usersync must provision the users and groups.

##### Active Directory by example

```yaml
---
# Which connector class to use
connector: AD

# some connector specific configuration 
configuration:
  connection:
    server: ad-01
    user: usersync
    password: mypass

# outbound rules.
# yet, usersync only support outbound rules!
outbound:

  # don't do anything with these remote objects
  whitelist:
    - uid=foo,dc=company,dc=tld
    - ou=sysaccounts,cn=accounts,dc=company,dc=tld

  # the keys in this section are not bound to any logic. So you can name it like you want. 
  # It is only important for the association.
  users:
    # connector specific configuration to sync things (in this case the users)
    
    # with the filter you can define which objects from your Objects should be synced.
    # so it is possible to sync only a special object kind (for example service users into a special ou)
    filter: user

    # it will generate an ldap object (because AD is ldap) with one objectClass "AccountObjectClass"
    # you can define multiple classes if needed
    objectClasses:
      - AccountObjectClass

    # this is the attribute mapping from your ObjectAttributes to the resource attributes.
    attributes:
      # syntax: attributeNameOfResource: attributeNameOfUsersync
      # sync the Usersync attribute "givenName" to the AD attribute "givenName"
      givenName: givenName
      familyName: familyName

      # sync the Usersync Attribute "userName" to the AD Attribute "sAMAccountName"
      sAMAccountName: userName

      # a special case:
      # every attribute can have a "code" configuration.
      # if this is found, Usersync will load the given perl class and call the method *outboundSync()*
      # the parameters to the method will be the complete user object from Usersync and the second parameters
      # will be the user object from the resource.
      # The method must return the value for the attribute. 
      cn:
        code: My::Shiny::Custom::LdapBuildUserCn

    # build associations
    # if you want to add users to their groups you have to define it here.
    association:
      # this will add the *cn* attribute of the user that is currently synced to the resource to 
      # the assigned group *member* attribute.

      # with: the key of the object which should be associated (see the "groups" entry)
      with: groups
      cn: member
      # direction: which way to link. With direction export, usersync will load the assigned group object
      # and add a new attribute to the group object (named member) and the value is the cn of the user object.
      direction: export

  # sync the groups (as above, this key has no meaning. it is only important for the association)
  groups:
    filter:
      - org
      - group
    objectClasses:
      - GroupObjectClass
    attributes:
      description: description
      cn:
        code: LdapBuildGroupCn
    association:
      with: users
      member: cn
      # direction: import ->
      # Usersync will load the assigned user object, get the *cn* of the user and add a new *member* attribute to the
      # group object with the *cn* of the user.
      direction: import
```


### How the software works

#### Syncronization process

The syncronization workflow is:

* Get all objects from the resource
* Find the difference between local resources and remote resources
* Generate an execution catalog
* Run hooks on execution catalog (like audit-logs)
* Give catalog to execution engine
* Execute the catalog
* Store the result

