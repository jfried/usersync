-- 
-- Created by SQL::Translator::Producer::PostgreSQL
-- Created on Thu Oct 27 21:19:27 2016
-- 
;
--
-- Table: attributes
--
CREATE TABLE "attributes" (
  "attribute_id" serial NOT NULL,
  "key" character varying(150) NOT NULL,
  "syntax" character varying(150) NOT NULL,
  PRIMARY KEY ("attribute_id")
);

;
--
-- Table: object_tree
--
CREATE TABLE "object_tree" (
  "object_tree_id" serial NOT NULL,
  "lft" integer NOT NULL,
  "rgt" integer NOT NULL,
  "level" integer NOT NULL,
  "tree_id" integer NOT NULL,
  "ref_id" integer NOT NULL,
  PRIMARY KEY ("object_tree_id"),
  CONSTRAINT "tree_id_unq" UNIQUE ("tree_id")
);
CREATE INDEX "object_tree_idx_tree_id" on "object_tree" ("tree_id");

;
--
-- Table: objects
--
CREATE TABLE "objects" (
  "object_id" serial NOT NULL,
  "name" character varying(150) NOT NULL,
  PRIMARY KEY ("object_id")
);

;
--
-- Table: object_attributes
--
CREATE TABLE "object_attributes" (
  "object_id" integer NOT NULL,
  "attribute_id" integer NOT NULL,
  PRIMARY KEY ("object_id", "attribute_id")
);
CREATE INDEX "object_attributes_idx_attribute_id" on "object_attributes" ("attribute_id");
CREATE INDEX "object_attributes_idx_object_id" on "object_attributes" ("object_id");

;
--
-- Table: object_configuration
--
CREATE TABLE "object_configuration" (
  "object_configuration_id" serial NOT NULL,
  "ref_id" integer NOT NULL,
  "object_id" integer NOT NULL,
  "attribute_id" integer NOT NULL,
  "value" character varying(1000) NOT NULL,
  PRIMARY KEY ("object_configuration_id")
);
CREATE INDEX "object_configuration_idx_attribute_id" on "object_configuration" ("attribute_id");
CREATE INDEX "object_configuration_idx_object_id" on "object_configuration" ("object_id");

;
--
-- Foreign Key Definitions
--

;
ALTER TABLE "object_tree" ADD CONSTRAINT "object_tree_fk_tree_id" FOREIGN KEY ("tree_id")
  REFERENCES "object_tree" ("tree_id") ON UPDATE CASCADE DEFERRABLE;

;
ALTER TABLE "object_attributes" ADD CONSTRAINT "object_attributes_fk_attribute_id" FOREIGN KEY ("attribute_id")
  REFERENCES "attributes" ("attribute_id") ON DELETE CASCADE ON UPDATE CASCADE DEFERRABLE;

;
ALTER TABLE "object_attributes" ADD CONSTRAINT "object_attributes_fk_object_id" FOREIGN KEY ("object_id")
  REFERENCES "objects" ("object_id") ON DELETE CASCADE ON UPDATE CASCADE DEFERRABLE;

;
ALTER TABLE "object_configuration" ADD CONSTRAINT "object_configuration_fk_attribute_id" FOREIGN KEY ("attribute_id")
  REFERENCES "attributes" ("attribute_id") ON DELETE CASCADE ON UPDATE CASCADE DEFERRABLE;

;
ALTER TABLE "object_configuration" ADD CONSTRAINT "object_configuration_fk_object_id" FOREIGN KEY ("object_id")
  REFERENCES "objects" ("object_id") ON DELETE CASCADE ON UPDATE CASCADE DEFERRABLE;

;
