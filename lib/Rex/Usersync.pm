package Rex::Usersync;
use Mojo::Base 'Mojolicious';
use Cwd 'getcwd';

use Rex::Usersync::Schema;

has schema => sub {
    my ($self) = @_;

    my $dsn =
        "dbi:Pg:"
      . "database="
      . $self->config->{database}->{schema} . ";" . "host="
      . $self->config->{database}->{host};

    return Rex::Usersync::Schema->connect(
        $dsn,
        $self->config->{database}->{username},
        $self->config->{database}->{password},
    );
};


# This method will run once at server start
sub startup {
  my $self = shift;

  #######################################################################
  # Define some custom helpers
  #######################################################################
  $self->helper( db => sub { $self->app->schema } );

  #######################################################################
  # Load configuration
  #######################################################################
  my @cfg = (
    getcwd() . "/usersync.conf",
    "/etc/rex/usersync.conf", "/usr/local/etc/rex/usersync.conf",
  );
  my $cfg;
  for my $file (@cfg) {
    if ( -f $file ) {
      $cfg = $file;
      last;
    }
  }

  #######################################################################
  # Load plugins
  #######################################################################
  if ($cfg) {
    $self->plugin( "Config", file => $cfg );
  }

  # Router
  my $r = $self->routes;

  # Normal route to controller
  $r->get('/')->to('example#welcome');
}

1;
