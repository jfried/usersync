#
# (c) Jan Gehring <jan.gehring@gmail.com>
#
# vim: set ts=2 sw=2 tw=0:
# vim: set expandtab:

package Rex::Usersync::Schema::Result::Attributes;

use strict;
use warnings;
use Moo;

use Data::Dumper;
extends qw(DBIx::Class::Core);

__PACKAGE__->load_components( 'InflateColumn::Serializer', 'PassphraseColumn',
    'Core' );

__PACKAGE__->table("attributes");

__PACKAGE__->add_columns(
    attribute_id => {
        data_type         => 'serial',
        is_auto_increment => 1,
        is_numeric        => 1,
    },
    key => {
        data_type   => 'varchar',
        size        => 150,
        is_nullable => 0,
    },
    syntax => {
        data_type   => 'varchar',
        size        => 150,
        is_nullable => 0,
    },
);

__PACKAGE__->set_primary_key('attribute_id');

__PACKAGE__->has_many( "object_configurations", "Rex::Usersync::Schema::Result::ObjectConfiguration",
    "attribute_id" );

__PACKAGE__->has_many( "object_attributes", "Rex::Usersync::Schema::Result::ObjectAttributes",
    "attribute_id" );

__PACKAGE__->many_to_many( objects => 'object_attributes', 'object_id');

1;
