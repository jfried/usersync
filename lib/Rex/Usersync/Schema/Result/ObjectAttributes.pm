#
# (c) Jan Gehring <jan.gehring@gmail.com>
#
# vim: set ts=2 sw=2 tw=0:
# vim: set expandtab:

package Rex::Usersync::Schema::Result::ObjectAttributes;

use strict;
use warnings;
use Moo;

use Data::Dumper;
extends qw(DBIx::Class::Core);

__PACKAGE__->load_components( 'InflateColumn::Serializer', 'PassphraseColumn',
    'Core' );

__PACKAGE__->table("object_attributes");

__PACKAGE__->add_columns(
    object_id => {
        data_type   => 'int',
        is_nullable => 0,
    },
    attribute_id => {
        data_type   => 'int',
        is_nullable => 0,
    },
);

__PACKAGE__->set_primary_key('object_id', 'attribute_id');

__PACKAGE__->belongs_to( "attribute", "Rex::Usersync::Schema::Result::Attributes",
    "attribute_id" );
__PACKAGE__->belongs_to( "object", "Rex::Usersync::Schema::Result::Objects",
    "object_id" );

1;
