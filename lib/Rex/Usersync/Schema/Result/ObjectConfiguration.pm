#
# (c) Jan Gehring <jan.gehring@gmail.com>
#
# vim: set ts=2 sw=2 tw=0:
# vim: set expandtab:

package Rex::Usersync::Schema::Result::ObjectConfiguration;

use strict;
use warnings;
use Moo;

use Data::Dumper;
extends qw(DBIx::Class::Core);

__PACKAGE__->load_components( 'InflateColumn::Serializer', 'PassphraseColumn',
    'Core' );

__PACKAGE__->table("object_configuration");

__PACKAGE__->add_columns(
    object_configuration_id => {
        data_type         => 'serial',
        is_auto_increment => 1,
        is_numeric        => 1,
    },
    ref_id => {
        data_type   => 'int',
        is_nullable => 0,
    },
    object_id => {
        data_type   => 'int',
        is_nullable => 0,
    },
    attribute_id => {
        data_type   => 'int',
        is_nullable => 0,
    },
    value => {
        data_type   => 'varchar',
        size        => '1000',
        is_nullable => 0,
    },
);

__PACKAGE__->set_primary_key('object_configuration_id');

__PACKAGE__->belongs_to( "attribute", "Rex::Usersync::Schema::Result::Attributes",
    "attribute_id" );
__PACKAGE__->belongs_to( "object", "Rex::Usersync::Schema::Result::Objects",
    "object_id" );

1;
