#
# (c) Jan Gehring <jan.gehring@gmail.com>
#
# vim: set ts=2 sw=2 tw=0:
# vim: set expandtab:

package Rex::Usersync::Schema::Result::ObjectTree;

use strict;
use warnings;
use Data::Dumper;
use Mojo::Util 'url_escape';
use Encode;
use Moo;

extends qw(DBIx::Class::Core);

__PACKAGE__->load_components(
    'InflateColumn::Serializer', 'Tree::NestedSet',
    'InflateColumn::DateTime',   'Core'
);

__PACKAGE__->table("object_tree");
__PACKAGE__->add_columns(
    object_tree_id => {
        data_type         => 'serial',
        is_auto_increment => 1,
        is_numeric        => 1,
    },
    lft => {
        data_type  => 'integer',
        is_numeric => 1,
    },
    rgt => {
        data_type  => 'integer',
        is_numeric => 1,
    },
    level => {
        data_type  => 'integer',
        is_numeric => 1,
    },
    tree_id => {
        data_type   => 'integer',
        is_numeric  => 1,
        is_nullable => 0,
    },
    ref_id => {
        data_type   => 'integer',
        is_numeric  => 1,
        is_nullable => 0,
        default     => 1,
    },
);

__PACKAGE__->set_primary_key(qw/object_tree_id/);

__PACKAGE__->add_unique_constraints(
  tree_id_unq => [ qw/tree_id/ ],
);

__PACKAGE__->tree_columns(
    {
        root_column  => 'tree_id',
        left_column  => 'lft',
        right_column => 'rgt',
        level_column => 'level',
    }
);

sub get_object_configuration {
  # links to object_configuration table via ref_id
}

1;