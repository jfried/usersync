#
# (c) Jan Gehring <jan.gehring@gmail.com>
#
# vim: set ts=2 sw=2 tw=0:
# vim: set expandtab:

package Rex::Usersync::Schema::Result::Objects;

use strict;
use warnings;
use Moo;

use Data::Dumper;
extends qw(DBIx::Class::Core);

__PACKAGE__->load_components( 'InflateColumn::Serializer', 'PassphraseColumn',
    'Core' );

__PACKAGE__->table("objects");

__PACKAGE__->add_columns(
    object_id => {
        data_type         => 'serial',
        is_auto_increment => 1,
        is_numeric        => 1,
    },
    name => {
        data_type   => 'varchar',
        size        => 150,
        is_nullable => 0,
    },
);

__PACKAGE__->set_primary_key('object_id');

__PACKAGE__->has_many( "object_configurations", "Rex::Usersync::Schema::Result::ObjectConfiguration",
    "object_id" );

__PACKAGE__->has_many( "object_attributes", "Rex::Usersync::Schema::Result::ObjectAttributes",
    "object_id" );

__PACKAGE__->many_to_many( attributes => 'object_attributes', 'attribute_id');

1;
